const rock = document.getElementById("rock")
const paper = document.getElementById("paper")
const scissors = document.getElementById("scissors")

function cpuResponse() {
    let result = Math.floor(Math.random() * 3);
    if (result === 1) {
        return "rock"
    } else if (result === 2) {
        return "paper"

    } else if (result === 3) {
        return "scissors"
    }
}

function display(output) {
    const destination = document.getElementById("destination")
    destination.innerHTML = output
}



rock.onclick = function() {

    if (cpuResponse() === "rock") {
        display("tie")

    } else if (cpuResponse() === "paper") {
        display("cpu wins")
    } else if (cpuResponse() === "scissors") {
        display("player 1 wins")
    }
}

paper.onclick = function() {

    if (cpuResponse() === "paper") {
        display("tie")

    } else if (cpuResponse() === "scissors") {
        display("cpu wins")
    } else if (cpuResponse() === "rock") {
        display("player 1 wins")
    }
}

scissors.onclick = function() {

    if (cpuResponse() === "scissors") {
        display("tie")

    } else if (cpuResponse() === "paper") {
        display("cpu wins")
    } else if (cpuResponse() === "rock") {
        display("player 1 wins")
    }
}